# RDMA-Tutorial
This project presents an example based tutorial on RDMA based programming. A more detailed 
discussion can be found on the [Wiki](https://github.com/jcxue/RDMA-Tutorial/wiki) page.

# RDMA-RC-example.c 
This example is from the "RDMA Aware Networks Programming User Manual v1.7" of Mellanox.

## Hardware and software requirement
 * Mellanox HCAs
 * GNU make
 * gcc-4.4
 * Mellanox OFED 3.3

## How to use

### build project
Simply use ```make``` to build the release version or ```make debug``` to build the 
debug version.

