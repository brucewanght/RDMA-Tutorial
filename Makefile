CC=gcc
CFLAGS=-Wall -Werror -O2
INCLUDES=
LDFLAGS=-libverbs
LIBS=-pthread -lrdmacm

SRCS1=main.c workload.c client.c config.c ib.c server.c setup_ib.c sock.c
SRCS2=RDMA_RC_example.c
OBJS1=$(SRCS1:.c=.o)
OBJS2=$(SRCS2:.c=.o)
PROG1=rdma-tutorial
PROG2=rdma-rc-example

all: $(PROG1) $(PROG2)

debug: CFLAGS=-Wall -Werror -g -DDEBUG
debug: $(PROG1)

.c.o:
	$(CC) $(CFLAGS) $(INCLUDES) -c -o $@ $<

$(PROG1): $(OBJS1)
	$(CC) $(CFLAGS) $(INCLUDES) -o $@ $(OBJS1) $(LDFLAGS) $(LIBS)

$(PROG2): $(OBJS2)
	$(CC) $(CFLAGS) $(INCLUDES) -o $@ $(OBJS2) $(LDFLAGS)

clean:
	$(RM) *.o *~ $(PROG1) $(PROG2)
